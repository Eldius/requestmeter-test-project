package net.eldiosantos.test.meter.runner;

import net.eldiosantos.test.meter.annotations.RequestConfig;
import net.eldiosantos.test.meter.annotations.helper.BrowserExtractor;
import net.eldiosantos.test.meter.annotations.helper.RequestConfigExtractor;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.RunListener;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Created by esjunior on 24/08/2015.
 */
public class CustomTestRunner extends BlockJUnit4ClassRunner {

    private final Map<Method, RequestConfig> testMethods;
    private final Map<Field, Class<? extends WebDriver>>browsersMap;
    private final Class testClass;

    private final BlockJUnit4ClassRunner runner;

    public CustomTestRunner(Class testClass) throws InitializationError {
        super(testClass);
        this.testClass = testClass;
        testMethods = new RequestConfigExtractor().extract(testClass);
        browsersMap = new BrowserExtractor().extract(testClass);
        runner = new BlockJUnit4ClassRunner(testClass) {

            private Statement executionResult;
            private EventFiringWebDriver driver;

            protected Statement withBefores(FrameworkMethod method, Object target, Statement statement) {

                System.out.println("Testing...");

                // init annotated browser
                browsersMap.forEach((f, b) -> {
                    try {
                        f.setAccessible(true);
                        driver = getBrowser(b);
                        System.out.println(String.format("browser: %s", driver.getClass().getCanonicalName()));
                        driver.get(method.getAnnotation(RequestConfig.class).url());
                        f.set(target, driver);
                        executionResult = super.withBefores(method, target, statement);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                });

                return executionResult;

            }

            @Override
            protected Statement withAfters(FrameworkMethod method, Object target, Statement statement) {
                //final File screenshotFile = driver.getScreenshotAs(OutputType.FILE);
                try {
                    //driver.close();
                } catch (NoSuchWindowException e) {
                    System.out.printf("Browser window alread closed.");
                }
                return super.withAfters(method, target, statement);
            }
        };
    }

    private EventFiringWebDriver getBrowser(Class<? extends WebDriver> browserClass) throws Exception {
        EventFiringWebDriver eventDriver = new EventFiringWebDriver(browserClass.newInstance());
        eventDriver.register(new AbstractWebDriverEventListener() {
            @Override
            public void beforeNavigateTo(String url, WebDriver driver) {
                super.beforeNavigateTo(url, driver);
                System.out.println(String.format("before: (%d) => url: %s", System.currentTimeMillis(), url));
            }

            @Override
            public void afterNavigateTo(String url, WebDriver driver) {
                super.afterNavigateTo(url, driver);
                System.out.println(String.format("after: (%d) => url: %s", System.currentTimeMillis(), url));
            }
        });

        return eventDriver;
    }

    @Override
    public Description getDescription() {
        final Description spec = Description.createSuiteDescription(
                testClass.getName()
                , this.testClass.getAnnotations()
        );
        return spec;
    }

    @Override
    public void run(RunNotifier notifier) {
        notifier.addListener(new RunListener());
        notifier.addFirstListener(new RunListener() {
            private Map<String, Long>startTime = new HashMap<String, Long>();
            private Map<String, Long>finishTime = new HashMap<String, Long>();
            @Override
            public void testStarted(Description description) throws Exception {
                super.testStarted(description);
                startTime.put(String.format("%s.%s", description.getClassName(), description.getMethodName()), System.currentTimeMillis());
            }

            @Override
            public void testFinished(Description description) throws Exception {
                super.testFinished(description);
                finishTime.put(String.format("%s.%s", description.getClassName(), description.getMethodName()), System.currentTimeMillis());
            }

            @Override
            public void testRunFinished(Result result) throws Exception {
                super.testRunFinished(result);
                startTime.keySet().forEach(k->System.out.println(String.format("duration: %s\t=>\t%d milisseconds", k, (finishTime.get(k) - startTime.get(k)))));
            }
        });
        runner.run(notifier);
    }
}
