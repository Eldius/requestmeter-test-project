package net.eldiosantos.test.meter.annotations.helper;

import net.eldiosantos.test.meter.annotations.Browser;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by esjunior on 25/08/2015.
 */
public class BrowserExtractor {

    public Map<Field, Class<? extends WebDriver>>extract(final Class<?>clazz) {
        return Arrays.asList(clazz.getDeclaredFields())
                .parallelStream()
                .filter(f->f.isAnnotationPresent(Browser.class))
                .collect(Collectors.toMap(f->f, f->f.getAnnotation(Browser.class).webdriver()));
    }
}
