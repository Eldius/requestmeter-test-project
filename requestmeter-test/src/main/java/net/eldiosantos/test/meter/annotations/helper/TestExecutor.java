package net.eldiosantos.test.meter.annotations.helper;

import net.eldiosantos.test.meter.annotations.RequestConfig;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by esjunior on 25/08/2015.
 */
public class TestExecutor {

    private final Map<Method, RequestConfig> testMethods;
    private final Map<Field, List<Class<? extends WebDriver>>>browsersMap;
    private final Class testClass;

    public TestExecutor(Map<Method, RequestConfig> testMethods, Map<Field, List<Class<? extends WebDriver>>> browsersMap, Class testClass) {
        this.testMethods = testMethods;
        this.browsersMap = browsersMap;
        this.testClass = testClass;
    }

    public Map<Method, List<Long>> execute() {
        final Map<Method, List<Long>>timesPerMethod = new HashMap<>();
        testMethods.forEach((m, c) -> {
            final List<Long>times = new ArrayList<Long>();
            browsersMap.forEach((f, b) -> {
                b.forEach(clazz -> {
                    try {
                        final Object testObj = testClass.newInstance();
                        f.setAccessible(true);
                        final WebDriver driver = clazz.newInstance();
                        final Long startTime = System.currentTimeMillis();
                        driver.get(c.url());
                        f.set(testObj, driver);
                        m.invoke(testObj);
                        times.add(System.currentTimeMillis() - startTime);
                        driver.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                });
            });

            timesPerMethod.put(m, times);
        });
        return timesPerMethod;
    }
}
