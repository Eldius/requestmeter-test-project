package net.eldiosantos.test.meter.annotations.helper;

import net.eldiosantos.test.meter.annotations.RequestConfig;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by esjunior on 25/08/2015.
 */
public class ExtractTestMethods {

    public Map<Method, RequestConfig>extract(Class<?>testClass) {
        return Arrays.asList(testClass.getDeclaredMethods()).stream()
                .filter(m->
                                m.getName().toUpperCase().startsWith("TEST")||
                                        (m.isAnnotationPresent(Test.class) &&
                                                m.isAnnotationPresent(RequestConfig.class))
                )
                .collect(Collectors.toMap(m->m, m->m.getAnnotation(RequestConfig.class)));
    }
}
