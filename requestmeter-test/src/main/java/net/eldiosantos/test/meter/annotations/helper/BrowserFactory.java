package net.eldiosantos.test.meter.annotations.helper;

import org.openqa.selenium.WebDriver;

/**
 * Created by esjunior on 25/08/2015.
 */
public class BrowserFactory {

    public WebDriver build(Class<? extends WebDriver> clazz) throws Exception {
        return clazz.newInstance();
    }
}
