package net.eldiosantos.test.meter.annotations.helper;

import net.eldiosantos.test.meter.annotations.RequestConfig;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by esjunior on 25/08/2015.
 */
public class RequestConfigExtractor {

    public Map<Method, RequestConfig>extract(final Class<?>clazz) {
        return Arrays.asList(clazz.getDeclaredMethods())
                .parallelStream()
                .filter(m -> m.isAnnotationPresent(RequestConfig.class))
                .collect(Collectors.toMap(m -> m, m -> m.getAnnotation(RequestConfig.class)));
    }
}
