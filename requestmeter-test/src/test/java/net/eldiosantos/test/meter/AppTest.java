package net.eldiosantos.test.meter;

import net.eldiosantos.test.meter.annotations.Browser;
import net.eldiosantos.test.meter.annotations.RequestConfig;
import net.eldiosantos.test.meter.runner.CustomTestRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

/**
 * Created by esjunior on 24/08/2015.
 */
@RunWith(CustomTestRunner.class)
public class AppTest {

    @Browser(webdriver = HtmlUnitDriver.class)
    private WebDriver webDriver;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    @RequestConfig(
            url="http://tokend.into.saude.gov.br"
            , calls = 1
            , threads = 1
    )
    public void testApp1() throws Exception {
        System.out.println(String.format("title: %s->%s", webDriver.getTitle(), webDriver.toString()));
        webDriver.findElement(By.id("login")).sendKeys("admin");
        webDriver.findElement(By.id("rawPass")).sendKeys("123Senha");
        webDriver.findElement(By.id("enter")).click();

        System.out.println(String.format("title: %s->%s", webDriver.getTitle(), webDriver.toString()));

        webDriver.close();
    }

    @Test
    @RequestConfig(
            url="http://igepd.into.saude.gov.br"
            , calls = 1
            , threads = 1
    )
    public void testApp2() throws Exception {
        System.out.println(String.format("title: %s->%s", webDriver.getTitle(), webDriver.toString()));
        webDriver.findElement(By.id("loginField")).sendKeys("admin");
        webDriver.findElement(By.id("passwdField")).sendKeys("123Senha");
        webDriver.findElement(By.cssSelector("#LoginPage > form > div.right > button")).click();

        System.out.println(String.format("title: %s->%s", webDriver.getTitle(), webDriver.toString()));

        webDriver.close();
    }
}
